package com.example.android.pets;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shams_Keshk on 09/09/17.
 */

public class PetViewHolder {

    @BindView(R.id.pet_name_text_view)
    TextView petName;
    @BindView(R.id.breed_name_text_view)
    TextView petBreedName;

    public PetViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}
